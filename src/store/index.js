import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    users: [],
    filteredUser: false
  },
  mutations: {
    SET_USER: (state, users) => {
      state.users = users;
    },
    FILTERED_USERS: (state, users) => {
      state.filteredUser = users;
    }
  },
  actions: {
    GET_USERS_FROM_API({ commit }) {
      return axios('https://reqres.in/api/users?per_page=-1', {
        method: 'GET'
      })
        .then((users) => {
          commit('SET_USER', users.data.data);
          return users;
        })
        .catch((error) => {
          console.log(error);
          return error;
        });
    }
  },
  getters: {
    GET_USERS(state) {
      return state.users;
    },
    USERS(state) {
      return (Array.isArray(state.filteredUser)) ? state.filteredUser : state.users;
    }
  }
});
